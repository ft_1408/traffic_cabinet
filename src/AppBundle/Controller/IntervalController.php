<?php
namespace AppBundle\Controller;

use Symfony\Component\Config\Definition\Exception\Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class IntervalController extends ApiController
{
    /**
     * @Route("/interval", name="interval_read")
     * @Method({"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function readAction(Request $request): JsonResponse
    {
        $intervalManager = $this->get('app.interval_manager');

        try {
            $intervals = $intervalManager->getIntervals($request);
        } catch (Exception $e) {
            return $this->getErrorResponse($e->getMessage());
        }

        return $this->getEntityToJsonResponse($intervals);
    }
}