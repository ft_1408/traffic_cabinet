<?php
namespace AppBundle\Controller;

use AppBundle\Entity\CreateEvent;
use AppBundle\Entity\Event;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use DateTime;
use InvalidArgumentException;

abstract class ApiController extends Controller
{
    const RESPONSE_SUCCESS = 'success';
    const RESPONSE_ERROR = 'error';

    /**
     * @return JsonResponse
     */
    protected function getSuccessResponse(): JsonResponse
    {
        return new JsonResponse(['status' => self::RESPONSE_SUCCESS]);
    }

    /**
     * @param string $errorMessage
     *
     * @return JsonResponse
     */
    protected function getErrorResponse(string $errorMessage): JsonResponse
    {
        return new JsonResponse(['status' => self::RESPONSE_ERROR, 'message' => $errorMessage], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @param array $entities
     *
     * @return JsonResponse
     */
    public function getEntityToJsonResponse(array $entities): JsonResponse
    {
        $normalizer = new GetSetMethodNormalizer();
        $normalizer->setIgnoredAttributes(['empty']);

        $serializer = new Serializer([new DateTimeNormalizer(DateTime::ISO8601), $normalizer], ['json' => new JsonEncoder()]);
        $json = $serializer->serialize($entities, 'json');

        return new JsonResponse($json, 200, [], true);
    }
}