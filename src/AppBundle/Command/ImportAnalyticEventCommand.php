<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportAnalyticEventCommand extends ContainerAwareCommand
{
    const COMMAND_NAME = 'app:import-analytic-event';

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->setDescription('Allows import google analytic events')
            ->setHelp('This command allows you to to import google analytic.');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $importService = $this->getContainer()->get('app.import.analytic_event_processor');
        $importService->run();
    }
}