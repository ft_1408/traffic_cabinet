<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Command\ImportAnalyticEventCommand;
use AppBundle\Command\ImportTrackEventCommand;
use AppBundle\Entity\Scheduler;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class LoadSchedulerData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @inheritdoc
     */
    public function load(ObjectManager $manager)
    {
        $scheduler = new Scheduler();
        $scheduler->setId(ImportAnalyticEventCommand::COMMAND_NAME);
        $scheduler->setStatus(Scheduler::STATUS_SUCCESS);
        $manager->persist($scheduler);
        $manager->flush();
    }

    /**
     * @inheritdoc
     */
    public function getOrder()
    {
        return 1;
    }
}