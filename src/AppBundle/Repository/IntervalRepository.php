<?php
namespace AppBundle\Repository;

use AppBundle\Entity\Interval;
use AppBundle\Service\IntervalManager;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;

class IntervalRepository extends EntityRepository
{
    /**
     * @param array $params
     *
     * @return Interval[]
     */
    public function findByParams(array $params): array
    {
        $expr = $this->getEntityManager()->getExpressionBuilder();
        $qb = $this->createQueryBuilder('s');

        if (!empty($params[IntervalManager::PARAM_PERIOD_BEGIN])) {
            $qb
                ->andWhere($expr->gte('s.dateImport', ':dateImportBegin'))
                ->setParameter(':dateImportBegin', $params[IntervalManager::PARAM_PERIOD_BEGIN], Type::INTEGER);
        }

        if (!empty($params[IntervalManager::PARAM_PERIOD_END])) {
            $qb
                ->andWhere($expr->lte('s.dateImport', ':dateImportEnd'))
                ->setParameter(':dateImportEnd', $params[IntervalManager::PARAM_PERIOD_END], Type::INTEGER);
        }

        if (!empty($params[IntervalManager::PARAM_AFFILIATE_ID])) {
            $qb
                ->andWhere($expr->eq('s.medium', ':affiliateId'))
                ->andWhere($expr->eq('s.source', ':source'))
                ->setParameter(':affiliateId', $params[IntervalManager::PARAM_AFFILIATE_ID], Type::INTEGER)
                ->setParameter(':source', IntervalManager::DEFAULT_AFFILIATE_SOURCE, Type::STRING);
        }

        return $qb->getQuery()->getResult();
    }
}