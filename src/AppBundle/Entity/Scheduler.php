<?php
namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table()
 */
class Scheduler
{
    const STATUS_SUCCESS = 1;
    const STATUS_ERROR = 2;
    const STATUS_RUNNING = 3;

    /**
     * @ORM\Column(type="string")
     * @ORM\Id
     *
     * @var string
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var DateTime
     */
    private $dateBegin;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var DateTime
     */
    private $dateEnd;

    /**
     * @ORM\Column(type="smallint", nullable=true, options={"unsigned"=true})
     *
     * @var integer
     */
    private $status;

    /**
     * @ORM\Column(type="smallint", nullable=true, options={"unsigned"=true})
     *
     * @var integer
     */
    private $mode;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }

    /**
     * @param DateTime $dateBegin
     */
    public function setDateBegin(DateTime $dateBegin)
    {
        $this->dateBegin = $dateBegin;
    }

    /**
     * @return DateTime
     */
    public function getDateBegin()
    {
        return $this->dateBegin;
    }

    /**
     * @param DateTime $dateEnd
     */
    public function setDateEnd(DateTime $dateEnd)
    {
        $this->dateEnd = $dateEnd;
    }

    /**
     * @return DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * @return integer
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param integer $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    /**
     * @return integer
     */
    public function getMode(): int
    {
        return $this->mode;
    }

    /**
     * @param integer $mode
     */
    public function setMode(int $mode)
    {
        $this->mode = $mode;
    }

}