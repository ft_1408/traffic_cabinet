<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\IntervalRepository")
 * @ORM\Table(name="`interval`")
 */
class Interval
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     *
     * @var integer
     */
    private $dateImport;

    /**
     * @ORM\Column(type="integer", options={"unsigned":true, "default":0})
     *
     * @var integer
     */
    private $pageViews;

    /**
     * @ORM\Column(type="integer", options={"unsigned":true, "default":0})
     *
     * @var integer
     */
    private $users;

    /**
     * @ORM\Column(type="integer", options={"unsigned":true, "default":0})
     *
     * @var integer
     */
    private $newUsers;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     *
     * @var double
     */
    private $bounceRate;

    /**
     * @ORM\Column(type="bigint", options={"unsigned":true, "default":0})
     *
     * @var integer
     */
    private $sessionDuration;

    /**
     * @ORM\Column(type="integer", options={"unsigned":true, "default":0})
     *
     * @var integer
     */
    private $transactions;

    /**
     * @ORM\Column(type="decimal", precision=6, scale=2)
     *
     * @var double
     */
    private $transactionsPerSession;

    /**
     * @ORM\Column(type="string", length=20)
     * @ORM\Id
     *
     * @var string
     */
    private $viewId;

    /**
     * @ORM\Column(type="string")
     * @ORM\Id
     *
     * @var string
     */
    private $source;

    /**
     * @ORM\Column(type="string")
     * @ORM\Id
     *
     * @var string
     */
    private $medium;

    /**
     * @return int
     */
    public function getPageViews(): int
    {
        return $this->pageViews;
    }

    /**
     * @param int $pageViews
     */
    public function setPageViews(int $pageViews)
    {
        $this->pageViews = $pageViews;
    }

    /**
     * @return int
     */
    public function getUsers(): int
    {
        return $this->users;
    }

    /**
     * @param int $users
     */
    public function setUsers(int $users)
    {
        $this->users = $users;
    }

    /**
     * @return int
     */
    public function getNewUsers(): int
    {
        return $this->newUsers;
    }

    /**
     * @param int $newUsers
     */
    public function setNewUsers(int $newUsers)
    {
        $this->newUsers = $newUsers;
    }

    /**
     * @return float
     */
    public function getBounceRate(): float
    {
        return $this->bounceRate;
    }

    /**
     * @param float $bounceRate
     */
    public function setBounceRate(float $bounceRate)
    {
        $this->bounceRate = $bounceRate;
    }

    /**
     * @return int
     */
    public function getSessionDuration(): int
    {
        return $this->sessionDuration;
    }

    /**
     * @param int $sessionDuration
     */
    public function setSessionDuration(int $sessionDuration)
    {
        $this->sessionDuration = $sessionDuration;
    }

    /**
     * @return int
     */
    public function getTransactions(): int
    {
        return $this->transactions;
    }

    /**
     * @param int $transactions
     */
    public function setTransactions(int $transactions)
    {
        $this->transactions = $transactions;
    }

    /**
     * @return float
     */
    public function getTransactionsPerSession(): float
    {
        return $this->transactionsPerSession;
    }

    /**
     * @param float $transactionsPerSession
     */
    public function setTransactionsPerSession(float $transactionsPerSession)
    {
        $this->transactionsPerSession = $transactionsPerSession;
    }

    /**
     * @return integer
     */
    public function getDateImport(): int
    {
        return $this->dateImport;
    }

    /**
     * @param integer $dateImport
     */
    public function setDateImport(int $dateImport)
    {
        $this->dateImport = $dateImport;
    }

    /**
     * @return string
     */
    public function getViewId(): string
    {
        return $this->viewId;
    }

    /**
     * @param string $viewId
     */
    public function setViewId(string $viewId)
    {
        $this->viewId = $viewId;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource(string $source)
    {
        $this->source = $source;
    }

    /**
     * @return string
     */
    public function getMedium(): string
    {
        return $this->medium;
    }

    /**
     * @param string $medium
     */
    public function setMedium(string $medium)
    {
        $this->medium = $medium;
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        $result = true;
        $result &= $this->pageViews == 0;
        $result &= $this->users == 0;
        $result &= $this->newUsers == 0;
        $result &= $this->bounceRate == 0;
        $result &= $this->sessionDuration == 0;
        $result &= $this->transactions == 0;
        $result &= $this->transactionsPerSession == 0;
        $result &= empty($this->source);
        $result &= empty($this->sourceMediumempty);

        return $result;
    }
}