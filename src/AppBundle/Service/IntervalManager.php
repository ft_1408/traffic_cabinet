<?php
namespace AppBundle\Service;

use AppBundle\Entity\Interval;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class IntervalManager
{
    const PARAM_PERIOD_BEGIN = 'period_begin';
    const PARAM_PERIOD_END = 'period_end';
    const PARAM_AFFILIATE_ID = 'affiliate_id';

    const DEFAULT_AFFILIATE_SOURCE = 'affiliate';

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * Constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Getting orders by allowed params setted in query
     *
     * @param Request $request
     *
     * @return Interval[]
     */
    public function getIntervals(Request $request): array
    {
        $allowedParams = [
            self::PARAM_PERIOD_BEGIN => $request->query->get(self::PARAM_PERIOD_BEGIN),
            self::PARAM_PERIOD_END => $request->query->get(self::PARAM_PERIOD_END),
            self::PARAM_AFFILIATE_ID => $request->query->get(self::PARAM_AFFILIATE_ID),
        ];

        $repository = $this->entityManager->getRepository('AppBundle:Interval');

        return $repository->findByParams($allowedParams);
    }
}