<?php
namespace AppBundle\Service\Google;

use AppBundle\Service\Import\Mapper;
use Exception;
use Google_Client;
use Google_Service_Analytics;
use Google_Service_Analytics_Profile;
use Google_Service_Analytics_Webproperty;

class Analytics
{
    const GOOGLE_ANALYTICS_READONLY_SCOPE = 'https://www.googleapis.com/auth/analytics.readonly';
    const GOOGLE_ANALYTICS_NAME = 'Google Analytics Reporting';

    /**
     * @var string
     */
    private $keyFile;
    /**
     * @var Google_Service_Analytics
     */
    private $provider;

    /**
     * @var string[]
     */
    private $profiles;

    /**
     * @var array
     */
    private $config;

    /**
     * Constructor.
     *
     * @param string $keyFile
     * @param array $config
     */
    public function __construct(string $keyFile, array $config)
    {
        $this->keyFile = $keyFile;
        $this->config = $config;
        $this->initAnalytics();
    }

    /**
     * Init google analytic object
     */
    private function initAnalytics()
    {
        $client = new Google_Client();
        $client->setApplicationName(self::GOOGLE_ANALYTICS_NAME);
        $client->setAuthConfig($this->keyFile);
        $client->setScopes([self::GOOGLE_ANALYTICS_READONLY_SCOPE]);
        $this->provider = new Google_Service_Analytics($client);

        $this->profiles = $this->getProfiles();
    }

    /**
     * Returns site view id for google analytics request
     *
     * @return string
     *
     * @throws Exception
     */
    private function getProfiles()
    {
        $profiles = [];

        // Get the user's first view (profile) ID.
        // Get the list of accounts for the authorized user.
        $accounts = $this->provider->management_accounts->listManagementAccounts();

        // Getting all accounts of authorized user
        // First menu in GA admin
        foreach ($accounts->getItems() as $account) {
            $accountId = $account->getId();

            // Get the list of properties for the authorized user.
            $properties = $this->provider->management_webproperties->listManagementWebproperties($accountId);
            // Second menu in GA admin
            foreach ($properties->getItems() as $item) {
                $profiles[] = $this->getProfile($item, $accountId);
            }

        }

        return $profiles;
    }

    /**
     * @param Google_Service_Analytics_Webproperty $item
     * @param string $accountId
     *
     * @return array
     *
     * @throws Exception
     */
    private function getProfile(Google_Service_Analytics_Webproperty $item, string $accountId): array
    {
        $propertyId = $item->getId();

        // Get the list of views (profiles) for the authorized user.
        $profiles = $this->provider->management_profiles
            ->listManagementProfiles($accountId, $propertyId);
        if (count($profiles->getItems()) > 0) {
            /**
             * @var $items Google_Service_Analytics_Profile[]
             */
            $items = $profiles->getItems();

            // Return the first view (profile) ID.
            // Third menu in GA admin
            $profile['id'] = $items[0]->getId();
            $profile['timezone'] = $items[0]->getTimezone();
            return $profile;

        } else {
            throw new Exception('No views (profiles) found for this user.');
        }
    }

    /**
     * @return array
     *
     * @throws Exception
     */
    public function getIntervals(): array
    {
        if (empty($this->profiles)) {
            throw new Exception('No profiles was found for this account');
        }

        $fields = implode(',', $this->config[Mapper::KEY_ANALYTICS_CONFIG_FIELDS]);
        $optionParams['dimensions'] = implode(',', $this->config[Mapper::KEY_ANALYTICS_CONFIG_DIMENSIONS]);
        $result = [];
        foreach ($this->profiles as $profile) {
            $item['timezone'] = $profile['timezone'];
            $item['response'] = $this->provider->data_ga->get(
                'ga:' . $profile['id'],
                $this->config[Mapper::KEY_ANALYTICS_CONFIG_PERIOD_BEGIN],
                $this->config[Mapper::KEY_ANALYTICS_CONFIG_PERIOD_END],
                $fields,
                $optionParams
            );
            $result[] = $item;
        }

        return $result;
    }
}