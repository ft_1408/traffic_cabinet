<?php
namespace AppBundle\Service\Import;

use AppBundle\Command\ImportAnalyticEventCommand;
use AppBundle\Entity\Scheduler;
use AppBundle\Service\Google\Analytics;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;

class AnalyticInterval
{
    /**
     * @var Analytics
     */
    private $analytic;

    /**
     * @var Mapper
     */
    private $mapper;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * AnalyticEvent constructor.
     *
     * @param Analytics $analytic
     * @param Mapper $mapper
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(Analytics $analytic, Mapper $mapper, EntityManagerInterface $entityManager)
    {
        $this->analytic = $analytic;
        $this->mapper = $mapper;
        $this->entityManager = $entityManager;
    }

    /**
     * Start import of events from google analytics
     */
    public function run()
    {
        $repository = $this->entityManager->getRepository('AppBundle:Scheduler');
        $scheduler = $repository->findOneBy(['id' => ImportAnalyticEventCommand::COMMAND_NAME, 'status' => Scheduler::STATUS_SUCCESS]);

        if (!empty($scheduler)) {
            $this->import($scheduler);
        }
    }

    /**
     * @param $scheduler
     */
    private function import(Scheduler $scheduler)
    {
        $scheduler->setDateBegin(new DateTime());
        $scheduler->setStatus(Scheduler::STATUS_RUNNING);

        $this->entityManager->flush();

        try {
            $googleResponses = $this->analytic->getIntervals();
            foreach ($googleResponses as $googleResponse) {
                $intervals = $this->mapper->getEntities($googleResponse);
                foreach ($intervals as $interval) {
                    if (!$interval->isEmpty()) {
                        $this->entityManager->merge($interval);
                    }
                }
            }
            $scheduler->setStatus(Scheduler::STATUS_SUCCESS);
        } catch (Exception $e) {
            $scheduler->setStatus(Scheduler::STATUS_ERROR);
        }

        $scheduler->setDateEnd(new DateTime());

        $this->entityManager->flush();
    }
}