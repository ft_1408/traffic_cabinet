<?php
namespace AppBundle\Service\Import;

use AppBundle\Entity\Interval;
use DateTime;
use DateTimeZone;
use InvalidArgumentException;

class Mapper
{
    const KEY_ANALYTICS_CONFIG_DIMENSIONS = 'dimensions';
    const KEY_ANALYTICS_CONFIG_FIELDS = 'fields';
    const KEY_ANALYTICS_CONFIG_PERIOD_BEGIN = 'period_begin';
    const KEY_ANALYTICS_CONFIG_PERIOD_END = 'period_end';

    /**
     * @var array
     */
    private $googleAnalyticConfig;

    /**
     * Constructor.
     *
     * @param array $googleAnalyticConfig
     */
    public function __construct(array $googleAnalyticConfig)
    {
        $this->googleAnalyticConfig = $googleAnalyticConfig;
    }

    /**
     * @param array $response
     *
     * @return Interval[]
     */
    public function getEntities(array $response): array
    {
        $result = [];
        $googleResponse = $response['response'];

        $rows = $googleResponse->getRows();
        $viewId = $googleResponse->getProfileInfo()->getWebPropertyId();
        $fields = array_merge($this->googleAnalyticConfig[self::KEY_ANALYTICS_CONFIG_DIMENSIONS], $this->googleAnalyticConfig[self::KEY_ANALYTICS_CONFIG_FIELDS]);

        // Check if period has some data otherwise exception will be thrown
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $result[] = $this->getEntity($fields, $row, $viewId, $response['timezone']);
            }
        }

        return $result;
    }

    /**
     * @param array $fields
     * @param array $row
     * @param string $viewId
     *
     * @return Interval
     */
    private function getEntity(array $fields, array $row, string $viewId, string $timezone): Interval
    {
        $indexOfHour = array_search('ga:hour', $this->googleAnalyticConfig[self::KEY_ANALYTICS_CONFIG_DIMENSIONS]);
        $indexOfDate = array_search('ga:date', $this->googleAnalyticConfig[self::KEY_ANALYTICS_CONFIG_DIMENSIONS]);
        $importDate = new DateTime($row[$indexOfDate], new DateTimeZone($timezone));
        $importDate->setTime($row[$indexOfHour], 0);

        $entity = new Interval();

        foreach ($fields as $index => $field) {
            // Skipping time data
            if ($index == $indexOfDate || $index == $indexOfHour) {
                continue;
            }

            $method = 'set' . ucfirst(str_replace('ga:', '', $field));
            if (method_exists($entity, $method)) {
                $entity->$method($row[$index]);
            } else {
                throw new InvalidArgumentException('Method ' . $method . ' not exists');
            }
        }

        $entity->setDateImport($importDate->getTimestamp());
        $entity->setViewId($viewId);
        return $entity;
    }
}